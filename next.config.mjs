/** @type {import('next').NextConfig} */
const nextConfig = {
    // images:{
    //     domains:["picsum.photos","avatars.githubusercontent.com"]
    // }
    images:{
        remotePatterns:[
            {
                protocol:"https",
                hostname:"**",
            }
        ]
    }
};

export default nextConfig;
