import DetailPost from "../components/DetailPost";

interface CardDataObj {
  _id: string;
  title: string;
  postImg: string;
  postText: string;
  catName: string;
  creator: {
    image: string;
    name: string;
  };
  createdAt: string;
  text: string[];
}

const DetailPage =async ({ params }: { params: { id: string } }) => {
  const res = await fetch(`http://localhost:3000/api/blog/${params.id}`, {cache: "no-cache"});
  const data: CardDataObj = await res.json();
  console.log(data);

  return (
    <div>
      <DetailPost key={data._id} id={params.id} postImg={data.postImg} catName={data.catName} title={data.title} postText={data.postText} creator={data.creator} createdAt={data.createdAt} />
    </div>
  );
};

export default DetailPage;
