import Image from "next/image";
import React from "react";
import moment from "moment"
interface PostItemProps {
  id: string;
  title: string;
  postImg: string;
  postText: string;
  catName: string;
  creator: {
    image: string;
    name: string;
  };
  createdAt: string;
}
const DetailPost = async ({ id,title,postImg, postText,catName, creator,createdAt  }: PostItemProps) => {
  const date = moment(createdAt).format('DD MMM, YYYY')
  return (
    <section>
      <div className="container max-w-[800px] mx-auto">
        <div className="pt-8">
          <button className="bg-indigo-500 text-white  rounded-md px-3 py-1 mb-6">{catName}</button>
          <h1 className=" text-4xl tracking-wide font-bold mb-5">{title}</h1>
          <div className="user flex items-center gap-5 mb-8">
            <div className="flex items-center gap-3">
              <Image src={creator.image} alt="post author image" width={36} height={36} className="rounded-full" />
              <p className="text-gray-400 leading-6">{creator.name}</p>
            </div>
            <p className="text-gray-400 leading-6">{date}</p>
          </div>
          <Image src={postImg} alt="image" width={800} height={300} loading="lazy" className="h-[300px] object-contain mb-8"/>
          {postText}
        </div>
      </div>
    </section>
  );
};

export default DetailPost;
