import moment from "moment";
import Image from "next/image";
import Link from "next/link";
interface PostItemProps {
  id: string;
  title: string;
  postImg: string;
  postText: string;
  catName: string;
  creator: {
    image: string;
    name: string;
  };
  createdAt: string;
}
const PostItem = ({
  id,
  title,
  postImg,
  postText,
  creator,
  createdAt,
  catName,
}: PostItemProps) => {
  const date = moment(createdAt).format('DD MMM, YYYY')
  return (
    <div>
      <div className=" rounded-lg p-4 border dark:bg-cardDark  dark:border-darkBorder">
        <Image
          src={postImg}
          width={360}
          height={240}
          alt="post image"
          className=" w-full rounded-md object-cover mb-4 h-[240px]"
        />
        <button className=" px-3 py-1 rounded-md bg-indigo-200 text-indigo-500 text-sm mb-4">
          {catName}
        </button>
        <Link
          href={id}
          className="line-clamp-3 text-2xl leading-7 font-semibold mb-5"
        >
          {title}
        </Link>
        <div className="user flex items-center gap-5">
          <div className="flex items-center gap-3">
            <Image
              src={creator.image}
              alt="post author image"
              width={36}
              height={36}
              className="rounded-full "
            />
            <p className="text-gray-400 leading-6">{creator.name}</p>
          </div>
          <p className="text-gray-400 leading-6">{date}</p>
        </div>
      </div>
    </div>
  );
};
export default PostItem;
